package edu.fontys.S4.view;

import com.jfoenix.controls.JFXButton;
import edu.fontys.S4.controller.JPAController;
import io.datafx.controller.ViewController;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

import javax.annotation.PostConstruct;

@ViewController(value = "/fxml/ui/JPABasic.fxml", title = "Basic window for controlling jpa entities")
public class JPABasicController  {
    // Find your JPA logic here
    JPAController control = new JPAController();

    @FXML
    private JFXButton btnConnect;

    @PostConstruct
    public void init() throws Exception{
        btnConnect.setOnMouseClicked((e) -> {
            control.Connect();
        });
    }
}
