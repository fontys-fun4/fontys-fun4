package edu.fontys.S4.view;

import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.svg.SVGGlyph;
import com.jfoenix.svg.SVGGlyphLoader;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Just the start up class for a javafx application. This class is also defined
 * in the build.gradle.
 */
public class JPAApplication extends Application {

    @FXMLViewFlowContext
    private ViewFlowContext flowContext;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Load Icomoon font set
        // If you want to change this, just change the file under resources/fonts
        new Thread(() -> {
            try {
                SVGGlyphLoader.loadGlyphsFont(JPAApplication.class.getResourceAsStream("/fonts/icomoon.svg"),
                        "icomoon.svg");
            } catch (IOException ioExc) {
                ioExc.printStackTrace();
            }
        }).start();

        Flow flow = new Flow(JPASampleController.class);
        DefaultFlowContainer container = new DefaultFlowContainer();
        flowContext = new ViewFlowContext();
        flowContext.register("Stage", primaryStage);
        flow.createHandler(flowContext).start(container);

        JFXDecorator decorator = new JFXDecorator(primaryStage, container.getView());
        decorator.setCustomMaximize(true);
        decorator.setGraphic(new SVGGlyph(""));

        primaryStage.setTitle("JPABasic Demo");

        double width = primaryStage.getWidth();
        double height = primaryStage.getHeight();
        try {
            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
            //width = bounds.getWidth() / 2.5;
            //height = bounds.getHeight() / 1.35;
        }catch (Exception e){ }

        Scene scene = new Scene(decorator, width, height);
        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(JPASampleController.class.getResource("/css/jfoenix-components.css").toExternalForm(),
//                JPASampleController.class.getResource("/css/jfoenix-design.css").toExternalForm(),
                JPASampleController.class.getResource("/css/jfoenix-main-demo.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
