package edu.fontys.S4.controller;

import edu.fontys.S4.model.Car;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAController {
    private EntityManager em;
    private EntityManagerFactory emf;

    public JPAController(){

    }

    public void Connect(){
        emf = Persistence.createEntityManagerFactory("jpaPU");
        em = emf.createEntityManager();

        Car car = new Car();
        car.setManufacturer("Ferrari");
        car.setModel("Modena");
        em.persist(car);
    }
}
