package edu.fontys.s4.jpaconnect;

import edu.fontys.s4.jpaconnect.model.Car;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAConnectBoot {

    public static void main(String[] args){
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("CarPU");
            EntityManager em = emf.createEntityManager();

            Car car= new Car();
            car.setManufacturer("Opel");
            car.setModel("Kadett");

            System.out.println("Storing the car");
            em.getTransaction().begin();
            em.persist(car);
            em.getTransaction().commit();

            System.out.println("Fethcing the cars");
            em.find(Car.class, car.getId());

            System.out.println(car.getManufacturer() + " " + car.getModel());

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
