import { observable } from "mobx";

class AppStore{
    id = 0;
    user = observable({ id: 0 });
    name = observable.box("");
    authenticated = observable.box(false);
    current = observable.box("start");

    host = "http://localhost:9000/se4-war-1.0-SNAPSHOT/rest/"; 
    loginUrl = this.host + "authentication/login";
    beerUrl = this.host + "beer/";

    logOut = () => {
        this.id = 0;
        this.user.id = 0;
        this.user.name = "";
        this.user.passHash="";
        this.name.set("")
        this.authenticated.set(false);
        this.current.set("start");
    }
}

export const appStore  = new AppStore();