import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { CardMedia, Card,CardContent, CardActions  } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText'
import { appStore } from '../Model/AppStore'

const styles = theme => ({
    card: {
      minWidth: 345,
    },
    media: {
      height: 140,
    },
    content: {
        flex: '1 0 auto',
      },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
      },
      details: {
        display: 'flex',
        flexDirection: 'column',
      },
});

class OverviewCard extends React.Component{
    
    render(){
        const { classes }= this.props;
        return(
            <Card className={classes.card}>
                <CardMedia
                    className={classes.media}
                    image="/media/images/overview.jpg"
                    title="Overzicht">
                </CardMedia>
                <div className={classes.details}>
                    <div className={classes.controls}>
                        <CardContent>
                        <Typography gutterBottom variant="headline" component="h2">
                            Welkom {appStore.name.get()}
                        </Typography>
                        </CardContent>
                    </div>
                    <div className={classes.controls}>
                    <CardActions>  
                        <List>
                            <ListItem button >
                                <ListItemText primary="Naar 'bieren'"/>                            
                            </ListItem>
                            <ListItem button>
                                <ListItemText primary="Naar 'Biersoorten'"/>                            
                            </ListItem>
                        </List>
                    </CardActions>
                    </div>
                </div>
            </Card>
        );
    }
}

OverviewCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(OverviewCard);