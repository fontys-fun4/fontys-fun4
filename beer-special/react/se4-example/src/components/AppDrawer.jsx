import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Typography, Icon } from '@material-ui/core';
import { AppDrawerList } from './AppDrawerList';
import { appStore } from '../Model/AppStore'



const styles = theme => ({
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
    toolbar: theme.mixins.toolbar,
    acoountPanel: {
        height: 150,
        color: 'white'
    }
  });

  class AppDrawer extends React.Component{      
      state = {
          opened : false,
          authorized : false,
      };

      toggleDrawer = (open) => () => {
          this.setState({ 
              "opened": open
            });
      }

      clickHandler = (screen) => () => {
          appStore.current.set(screen);
      }

      render(){
        const { classes } = this.props;
        appStore.authenticated.observe(
            (auth) => {
                console.log(auth);
                this.setState({ authorized : auth.newValue});
            }
        );          

          const sideList = (
            <div className={classes.list}>                
                <List>{AppDrawerList}</List>
                <Divider />
            </div>
          );

          return(
            <div>
                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.toggleDrawer(true)}>
                    <MenuIcon />
                </IconButton>
                <Drawer open={this.state.opened} onClose={this.toggleDrawer( false)}>
                    <div className={classes.toolbar} style={{
                        background: 'url(media/images/user1.jpg)',
                        backgroundSize: 'cover', 
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',}}>                
                        <Typography style={{ color: "white"}}>
                            <Icon>account_circle</Icon>
                        </Typography>
                        <Typography className={classes.acoountPanel}>
                            {appStore.name.get()}
                        </Typography>
                    </div>                
                    <div
                    tabIndex={0}
                    role="button"
                    onClick={this.toggleDrawer( false)}
                    onKeyDown={this.toggleDrawer( false)}
                >

                    <Divider/>
                    {this.state.authorized ?
                     <div>{sideList}</div>:
                     <div className={classes.list}>                
                     </div>
                    }
                </div>
                </Drawer>         
            </div>
          )
      }
  }
  
  AppDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(AppDrawer);