import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AppDrawer from './AppDrawer'
import { appStore } from '../Model/AppStore'

/** 
 * This style is used for the default rendering of you custom component.
 * 
 */
const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class BeerBar extends React.Component{
    // You need this to store your components state
    state = {        
        auth:false,
    };

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root} >
                <AppBar position="static">
                    <Toolbar>
                        {/* The menu button or 'hamburger' */}
                        <AppDrawer></AppDrawer>                
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Beer Review App
                        </Typography>
                    </Toolbar>
                    {/* This is only visible when authenticated */ }
                    {appStore.authenticated.get() && (
                        <div>                            
                        </div>
                    )}
                </AppBar>
             </div>
        )

    }
}

BeerBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(BeerBar);