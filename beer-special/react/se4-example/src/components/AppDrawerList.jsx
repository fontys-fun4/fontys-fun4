import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import Icon from '@material-ui/core/Icon';
import { Divider }  from '@material-ui/core';
import BeerIcon  from './BeerIcon';
import { appStore } from '../Model/AppStore';

const styles = theme => ({
icon: {
    margin: theme.spacing.unit * 2,
  }
});

const clickHandler = (screen) => () => {
    appStore.current.set(screen);
}

export const AppDrawerList = (  
        <div>
            <ListItem button onClick={() => clickHandler('beer')}>
                <ListItemIcon>
                <BeerIcon color="primary"></BeerIcon>            
                </ListItemIcon>
                <ListItemText primary="Bieren" />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <Icon color="primary">account_circle</Icon>
                </ListItemIcon>
                <ListItemText primary="Bier Soorten" />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <Icon color="primary">account_circle</Icon>
                </ListItemIcon>
                <ListItemText primary="Bars" />
            </ListItem>
            <Divider/>
            <ListItem button onClick={() =>  appStore.logOut()} >
                <ListItemIcon>
                    <Icon color="primary">account_circle</Icon>
                </ListItemIcon>
                <ListItemText primary="Afmelden" />
            </ListItem>        
        </div>    
)