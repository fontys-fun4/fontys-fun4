import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { appStore } from '../Model/AppStore'

const styles = theme => ({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class LoginCard extends React.Component{
  loginImage = "";
  state = {
    userName: '',
    password: '',
    auth: false
  };

  constructor(props){
    super(props);
    this.state.userName = '';
    this.loginImage = "/media/images/beer" + 
                        (Math.floor(Math.random() * (2)) + 1) + 
                      ".jpg";
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleLoginClick = name => event => {
    var body = [];
    body.push(encodeURIComponent("user") + "=" + encodeURIComponent(this.state.userName));
    body.push(encodeURIComponent("pass") + "=" + encodeURIComponent(this.state.password));
    body = body.join("&");

    fetch(appStore.loginUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'origin': "http://localhost:9000"
      },
      body: body,
    })
    .then(res => res.json())
    .catch((msg) => console.log(msg))
    .then(
      res => {
                if(res !== undefined && 'id' in res)
                {
                    appStore.user = res;
                    appStore.name.set(res.name);
                    appStore.authenticated.set(true);
                    appStore.current.set('beer');
                }
                else{
                    alert(res);
                }
          
        }
      );
    }    

  render(){
    const { classes }= this.props;
    // Take a random image from the images galery

    return (        
        (!appStore.authenticated.get() && (<Card className={classes.card}>
          <CardMedia
            className={classes.media}
            image={this.loginImage}
            title="Login"
          />
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              Login
            </Typography>
            <form className={classes.container} noValidate autoComplete="off">
              <TextField
                id="name"
                label="Gebruikersnaam"
                className={classes.textField}
                value={this.state.userName}
                onChange={this.handleChange('userName')}
                margin="normal"
              />
              <TextField
                id="password-input"
                label="Password"
                className={classes.textField}
                onChange={this.handleChange('password')}
                type="password"
                margin="normal"
              />
            </form>
          </CardContent>
          <CardActions>
            <Button size="small" color="primary" onClick={this.handleLoginClick()}>
              Login
            </Button>
            <Button size="small" color="primary">
              Register
            </Button>
          </CardActions>
        </Card>
      )));
  }
}

LoginCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LoginCard);