import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import ListSubheader from '@material-ui/core/ListSubheader';
import { appStore } from '../Model/AppStore';
import BeerTile from './BeerTile'

const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    papaerRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    gridList: {
      width: 500,
      height: 450,
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)',
    },
  });


class BeerGrid extends React.Component{    
    state = { beerList: [] }

    handleTileClick= (beer) => {
        alert(beer.name);
    }

    componentWillMount(){
        /**
         * Tries to get the beer list from the rest service.
         */
        
        fetch(appStore.beerUrl + "beers",{
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
              'origin': "http://localhost:9000"
            }
          })
          // Get the content as JSon
          .then((res) => res.json())
          // Update the views
          .then((res) => {
              this.setState({ beerList : res})
              this.forceUpdate();
          })
    }

    render(){
        const {classes } = this.props;

        return(
            <div className={classes.root} >
                <GridList className={classes.gridList}>
                    <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                        <ListSubheader component="div">De bieren</ListSubheader>
                    </GridListTile>
                    {this.state.beerList.map(beer => (    
                        <BeerTile beer={beer} onClick={() => this.handleTileClick(beer)}>
                        </BeerTile>                    
                    ))}
                </GridList>
            </div>
        );
    }
}

BeerGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BeerGrid);
