import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import './App.css';
import BeerBar from './components/BeerBar';
import LoginCard from './components/LoginCard';
import View from 'react-flexbox';
import BeerGrid from './components/BeerGrid';
import OverviewCard from './components/OverviewCard';
import { observer } from 'mobx-react';
import { appStore } from './Model/AppStore';
// import { reaction } from 'mobx';

const styles = {
  content:{
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    width: 'auto', 
    margin: 15
  },
  login:{
    margin: 15,
  }
};

class App extends Component {

  state = { 
    authenticated : false
  }
  
  authListener = (auth) => {
    this.setState( { authenticated: auth.newValue });
  };

  componentDidMount(){
    appStore.authenticated.observe(this.authListener);
  }

  beerScreen = <BeerGrid></BeerGrid>
  
  startScreen = <View style={ styles.content }>
                  {
                    this.state.authenticated ?         
                    (<OverviewCard></OverviewCard>) :
                    (<LoginCard style={ styles.login }></LoginCard>) 
                  }
                </View>

  screens = { 
    'start' : this.startScreen,
    'beer' : this.beerScreen
  }

  render() {
    return (
      <div className="App">      
        <BeerBar></BeerBar>
        {this.screens[appStore.current]}
      </div>
    );
  }
}

export default withStyles(styles)(observer(App));
