package edu.fontys.se4.main;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

import java.io.File;

public class Bootstrap {
    /**
     * Executes the payara micro profile engine with the war specified in the program arguments
     * @param args
     * @throws BootstrapException
     */
    public static void main(String[] args) throws BootstrapException
    {
        String warPath = "se4-war-1.0-SNAPSHOT.war";
        if(args.length > 0)
        {
            warPath = args[0];
        }

        PayaraMicro micro = PayaraMicro.getInstance()
                .setHttpPort(9000)
                .addDeployment(warPath);
        micro.bootStrap();
    }
}
