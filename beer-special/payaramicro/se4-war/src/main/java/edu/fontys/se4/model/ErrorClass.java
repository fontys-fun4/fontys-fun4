package edu.fontys.se4.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorClass extends JsonType {
    private String name;
    private String message;

    public ErrorClass(){
        setTag("error");
    }

    public ErrorClass(String name,String message){
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
