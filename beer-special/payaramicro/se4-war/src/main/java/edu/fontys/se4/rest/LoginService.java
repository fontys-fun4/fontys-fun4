package edu.fontys.se4.rest;

import com.google.gson.Gson;
import edu.fontys.se4.controller.AuthenticationController;
import edu.fontys.se4.model.ErrorClass;
import edu.fontys.se4.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/authentication")
public class LoginService {
    private AuthenticationController authControl= new AuthenticationController();

    /**
     * Returns a json object that descibes this rest.
     * @return The string with the json object
     */
    @GET
    public String defaultGet(){
        return "{" +
                "name: 'BeerApplication Login rest," +
                "description: 'Some default action for this rest'"+
                "}";
    }

    /**
     *
     * @param user
     * @return
     */
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String logon(
            @FormParam("user") String user,
            @FormParam("pass") String pass){
        User result = null;
        Gson gson = new Gson();
        if((result = authControl.logon(user, pass)) != null){
            return gson.toJson(result);
        }
        else
        {
            String msg = gson.toJson(new ErrorClass("Error login", "Gebruikersnaam of wachtwoord onbekend"));
            Response.status(403);
            return msg;
        }
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public void register(User user) {
        authControl.register(user);
    }
}
