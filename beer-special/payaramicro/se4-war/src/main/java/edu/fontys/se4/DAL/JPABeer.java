package edu.fontys.se4.DAL;

import edu.fontys.se4.model.Beer;

import javax.persistence.EntityManager;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import java.util.List;

public class JPABeer implements IsBeer {
    private EntityManager em;

    public JPABeer(EntityManager em){
        this.em = em;
    }

    @Override
    public Beer getBeer(int id) {
        Beer result = null;
        result = (Beer)em.find(Beer.class,(long) id);
        return result;
    }

    @Override
    public List<Beer> getAllBeer() {
        Query qry = em.createNamedQuery("Beer.getAll");
        return qry.getResultList();
    }

    /**
     * Searches for the searchTerms in name and description of the beertable.
     * @param searchTerm The search term
     * @return A list of beers which mets the searchterms.
     */
    @Override
    public List<Beer> searchBeer(String searchTerm) {
        Query qry = em.createNamedQuery("Beer.search");
        return qry.getResultList();
    }

    /**
     * Updates the beer if the transaction is active.
     * @param beer The beer to update
     */
    @Override
    public void Update(Beer beer) {
        if(em.getTransaction().isActive()){
            em.merge(beer);
        }

    }

    /**
     * Stores the new beer.
     * @param beer The beer to store.
     */
    @Override
    public void Store(Beer beer) {
        if(em.getTransaction().isActive()){
            em.persist(beer);
        }
    }
}
