package edu.fontys.se4.rest;

import edu.fontys.se4.controller.AuthenticationController;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/rest")
public class BeerApplication extends Application {
    AuthenticationController authControl;

    @PostConstruct
    public void init(){
        authControl  = new AuthenticationController();

        /**
         * Start a new thread to create the default values in the database.
         */
        new Thread(){
            public void run(){
                new edu.fontys.se4.controller.BeerController().createBeerTypes();

            }
        }.start();
    }

    /**
     * Geeft een set met classes voor de bier rest terug
     * @return Een json object.
     */
    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(
                Arrays.asList(
                        BeerService.class,
                        LoginService.class,
                        BeerCorsFilter.class
                        ));
    }

}
