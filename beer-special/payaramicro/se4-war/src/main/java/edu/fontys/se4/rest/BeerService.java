package edu.fontys.se4.rest;

import edu.fontys.se4.model.Beer;
import edu.fontys.se4.model.BeerType;
import jdk.nashorn.internal.objects.annotations.Getter;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceException;
import java.io.*;
import java.net.URL;
import java.util.List;

@Path("/beer")
public class BeerService {
    edu.fontys.se4.controller.BeerController beerController = new edu.fontys.se4.controller.BeerController();

    /**
     * Geeft een beschrijving terug voor deze rest.
     * @return Een json object met informatie over de beerservice
     */
    @GET
    public String defaultGet(){
        return "{" +
                "name: 'BeerApplication beer rest," +
                "description: 'Hier haal je de informatie over het bier op.'," +
                "remarks: 'Deze functie is gemaakt om te testen of de rest werkt." +
                "}";
    }

    @POST
    @Path("/new")
    @Consumes(MediaType.APPLICATION_JSON)
    public void newBeer(Beer beer){

    }

    /**
     * Returns the list of beers
     * @return the lis of beers.
     */
    @GET
    @Path("/beers")
    public List<Beer> getBeers(){
        return beerController.getAllBeers();
    }

    /**
     * Returns the list of beertypes
     * @return A array of beertypes
     */
    @GET
    @Path("/types")
    @Produces(MediaType.APPLICATION_JSON)
    public List<BeerType> getBeerTypes(){
        return beerController.getAllTypes();
    }

    /**
     * Gets an image based on the id of the beer. The image file name will be fetched
     * from the database
     * @param id Unique identifier of the beer
     * @return An image in the repsonse.
     */
    @GET
    @Path("/image/{id}")
    public Response getImage(@PathParam("id")int id){
        File image;
        Response response;

        Beer beer = beerController.getBeer(id);
        URL url = this.getClass().getResource("/images/" + beer.getImage().toString());


        if(url == null){
            throw new WebApplicationException("Beer image not found",404);
        }

        image = new File(url.getFile());
        return Response
                .ok(image)
                .header("Content-Disposition", "attachment; filename=" +  image.getName())
                .build();
    }
}
