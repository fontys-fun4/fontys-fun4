package edu.fontys.se4.DAL;
import edu.fontys.se4.model.BeerType;

import java.lang.System;
import java.util.List;
import java.util.Locale;

/**
 * Interface for setting the basic CRUD Handling for beertype
 */
public interface IsBeerType {
    /**
     * Returns the beertype with [id] or null
     * @param id The Id of the beertype
     * @return The beertype or null if id is unknown
     */
    BeerType getType(int id);

    /**
     * Returns a list of all Beertypes available in the database
     * @return A list of beertypes
     */
    List<BeerType> getAllType();

    /**
     * Updates the beertype if the type already exists
     * @param type The beertype to be updated.
     */
    void update(BeerType type);

    /**
     * Creates a new BeerType by storing it into the database.
     * @param type The new beertype to be stored.
     */
    void store(BeerType type);
}