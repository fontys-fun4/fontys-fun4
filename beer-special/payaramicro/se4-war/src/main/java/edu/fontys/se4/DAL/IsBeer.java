package edu.fontys.se4.DAL;

import edu.fontys.se4.model.Beer;

import java.util.List;

/**
 * Defines a set of methods to fetch, store and manipulate data from the database
 */
public interface IsBeer {
    /**
     * Gets the beer with [id]
     * @param id The id of the beer to fetch
     * @return A Beer object or null if id is unknown.
     */
    Beer getBeer(int id);

    /**
     * Returns a list of all available beer in the database
     * @return A list of beers.
     */
    List<Beer> getAllBeer();

    /**
     * Searches for beer with the given searchterm
     * @param searchTerm The search tem
     * @return A list of beers that meets the searchTerms.
     */
    List<Beer> searchBeer(String searchTerm);

    /**
     * Updates a already existing beer.
     * @param beer The beer to update
     */
    void Update(Beer beer);

    /**
     * Stores a new beer in to the database.
     * @param beer The beer to store.
     */
    void Store(Beer beer);

}
