package edu.fontys.se4.controller;

import edu.fontys.se4.DAL.IsBeer;
import edu.fontys.se4.DAL.IsBeerType;
import edu.fontys.se4.DAL.JPABeer;
import edu.fontys.se4.DAL.JPABeerType;
import edu.fontys.se4.model.Beer;
import edu.fontys.se4.model.BeerType;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class BeerController {
    /**
     * The entitymanager that connects to the database
     */
    private EntityManager em;

    /**
     * The persistence unit.
     */
    private String puKey = "beerPU";

    private IsBeerType jpaBeerType;
    private IsBeer jpaBeer;

    public BeerController(){
        try {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory(puKey);
            em = factory.createEntityManager();

            jpaBeerType = new JPABeerType(em);
            jpaBeer = new JPABeer(em);
        }
        catch(Exception ex){
            // TODO: create some kind of logging.
            ex.printStackTrace();
        }
    }

    /**
     * Returns a list of beertypes stored in the database.
     * @return A list of beertypes.
     */
    public List<BeerType> getAllTypes(){
        return jpaBeerType.getAllType();
    }

    /**
     * Returns a list of all beers
     * @return Lis of all beers.
     */
    public List<Beer> getAllBeers(){
        return jpaBeer.getAllBeer();
    }

    public Beer getBeer(int id){
        return jpaBeer.getBeer(id);
    }

    /**
     * Creates a default list of beers.
     */
    public void createBeerTypes(){
        em.getTransaction().begin();

        try {

            // create the beer types
            BeerType ipa = createType(
                    "Indian Pale Ale",
                    "\"India Pale Ale, afgekort IPA, is een bierstijl binnen de categorie Pale ale. Het is een extra hoppig en (vaak) extra bitter bier[1] en werd voor het eerst gebrouwen in Engeland in de 19e eeuw en was bestemd voor de Londense markt.\"");
            createType(
                    "Session Ale",
                    "Zo weinig mogelijk alcohol, zoveel mogelijk aroma en smaak. Stevig gehopt wat de lage moutstorting grotendeels compenseert. Have another one!"
                    );
            BeerType stout = createType(
                    "Stout",
                    "Stout is een biersoort die wordt gekenmerkt door een donkerbruine tot zwarte kleur en een enigszins branderig bittere smaak door het gebruik van gebrande of geroosterde mout. Er bestaan zowel hooggegiste als laaggegiste varianten."
            );
            createType(
                    "Pils",
                    "Pils (ook pilsener of pilsner genoemd) is een soort bier dat voor het eerst door de Duitser Josef Groll in de stad Pilsen (Tsjechisch: Plzeň) in het voormalige Oostenrijk-Hongarije en het huidige Tsjechië werd gebrouwen. De naam is afgeleid van de naam van de stad. Pils bevat ongeveer 5% alcohol. Het is een ondergistend bier met een goudgele kleur. Pils komt het beste tot zijn recht bij een temperatuur van 4 graden Celsius."
            );

            // create the beers
            createBeer("Punk IPA", "Our scene-stealing flagship is an India Pale Ale that has become a byword for craft beer rebellion; synonymous with the insurgency against mass-produced, lowest common denominator beer. Punk IPA charges the barricades to fly its colours from the ramparts – full-on, full-flavour; at full-throttle.\n" +
                    "\n" +
                    "Layered with new world hops to create an explosion of tropical fruit and an all-out riot of grapefruit, pineapple and lychee before a spiky bitter finish, this is transatlantic fusion running at the fences of lost empires.\n" +
                    "\n" +
                    "Nothing will ever be the same again",
                    "1502306606bot_punk.png",
                    ipa);

            createBeer( "HEART",
                    "Flaked oats and wheat add to the velvet mouth-feel, with the carbonation adding a honeycomb texture. Magnum and Sorachi Ace bring a berry & citrus fruitiness that amplifies the chocolate character of this inky leviathan",
                    "1502306638bot_jetblack.png",
                    stout);

            em.getTransaction().commit();
        }
        catch(Exception ex){
            em.getTransaction().rollback();

            // TODO: create some logging system.
            ex.printStackTrace();
        }
    }

    /**
     * Helper method to help storing the beertypes in the database.
     * @param name name of the beertype
     * @param description the description to store.
     */
    private BeerType createType(String name, String description){
        BeerType type = new BeerType();
        type.setName(name);
        type.setDescription(description);
        jpaBeerType.store(type);
        return type;
    }

    private void createBeer(String name, String description, String image, BeerType type){
        Beer beer = new Beer();
        beer.setName(name);
        beer.setImage(image);
        beer.setDescription(description);
        beer.setType(type);
        jpaBeer.Store(beer);
    }
}
