package edu.fontys.se4.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public abstract class JsonType {
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
