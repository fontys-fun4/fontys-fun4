package edu.fontys.se4.model;

import javax.persistence.*;
import java.util.List;

/**
 * The logged on user
 */
@Entity
@NamedQueries(
        @NamedQuery(name= "User.Logon", query = "select u from User u where u.name = :name and u.passHash = :pass")
)
public class User extends JsonType{

    @Id
    @GeneratedValue
    private long id;

    private String name;
    private String passHash;
    private String status;

    @OneToMany
    private List<Beer> beer;

    public User(){
        super();
        setTag("user");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
