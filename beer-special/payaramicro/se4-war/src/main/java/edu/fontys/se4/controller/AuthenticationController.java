package edu.fontys.se4.controller;


import com.sun.media.jfxmedia.logging.Logger;
import edu.fontys.se4.model.User;

import javax.persistence.*;

public class AuthenticationController {

    private EntityManager em;

    public AuthenticationController(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("beerPU");
        em = factory.createEntityManager();

        User user = em.find(User.class, 1L);
        if(user == null){
            user = new User();
            user.setName("defjef");
            user.setPassHash("Jeffm@n01");
            user.setStatus("Created by the server as default");
            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();
        }
    }

    /**
     * Tries to logon on a user by the given username and password
     * @param user The user name
     * @param pass The passowrd
     * @return The logged on user or null if no user could be found.
     */
    public User logon(String user, String pass){
        User dbUser;
        Query qry = em.createNamedQuery("User.Logon");
//        Logger.logMsg(Logger.INFO, "Login for user: " + user);
        System.out.println("Login for user: " + user);
        qry.setParameter("name", user);
        qry.setParameter("pass", pass);
        try {
            dbUser = (User) qry.getSingleResult();
        }
        catch(NoResultException nre) {
            nre.printStackTrace();
            dbUser = null;
        }

        return dbUser;

    }

    /**
     * Registers a user into the database
     * @param user The user to register.
     */
    public void register(User user){
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }
}
