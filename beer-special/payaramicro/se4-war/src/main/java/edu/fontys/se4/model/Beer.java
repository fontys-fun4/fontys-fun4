package edu.fontys.se4.model;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Beer.getAll", query = "select b from Beer b"),
        @NamedQuery(name = "Beer.search", query = "select b from Beer b where b.description like concat('%',:condition,'%') or b.name like concat('%',:condition,'%')")
})
public class Beer {

    @Id
    @GeneratedValue
    private long Id;

    private String name;
    private String image;

    @ManyToOne
    private BeerType type;

    @Lob
    @Column(length = 2048)
    private String description;

    @OneToMany
    private List<Review> reviews;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BeerType getType() {
        return type;
    }

    public void setType(BeerType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }
}
