package edu.fontys.se4.DAL;

import edu.fontys.se4.model.BeerType;

import javax.persistence.EntityManager;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class JPABeerType implements IsBeerType {
    /**
     * The entitymanager for access to a database
     */
    private EntityManager em;

    /**
     * Constructor which accepts an entitymanager for accessing the database.
     * @param em
     */
    public JPABeerType(EntityManager em){
        this.em = em;
    }

    @Override
    public BeerType getType(int id) {
        BeerType result = null;
        result = (BeerType)em.find(result.getClass(), id);
        return result;
    }

    /**
     * Returns a list of beertypes from the database.
     * @return a list of beertypes.
     */
    @Override
    public List<BeerType> getAllType() {
        Query getAllQuery = em.createNamedQuery("BeerType.getAll");
        return getAllQuery.getResultList();
    }

    /**
     * Stores the beertype if a transaction is available.
     * @param type The beertype to be updated.
     */
    @Override
    public void update(BeerType type) {
        if(em.getTransaction().isActive()) {
            em.merge(type);
        }
    }

    /**
     * Persits the Beertype if the transaction is available.
     * @param type The new beertype to be stored.
     */
    @Override
    public void store(BeerType type) {
        if(em.getTransaction().isActive()){
            em.persist(type);
        }
    }
}
