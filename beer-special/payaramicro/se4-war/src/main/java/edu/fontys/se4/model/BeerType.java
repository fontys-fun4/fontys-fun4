package edu.fontys.se4.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@NamedQueries(
        @NamedQuery(name="BeerType.getAll", query = "select bt from BeerType bt")
)
@XmlRootElement
public class BeerType {
    private String name;

    @Lob
    @Column(length = 2048)
    private String description;

    @Id
    @GeneratedValue
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
